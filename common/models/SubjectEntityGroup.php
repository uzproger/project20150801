<?php 

namespace common\models;

use yii\helpers\ArrayHelper;


/**
 * Subject groups
 */
class SubjectEntityGroup extends EntityGroup
{
    const GROUP_COLLEGE = 'college';
    const GROUP_LUCEUM = 'lyceum';
    const GROUP_SCHOOL = 'school';
    const GROUP_UNIVERSITY = 'university';

    /**
     * @inheritdoc
     */         
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [

        ]);
    }

    /**
     * Returns subject groups
     * @return array
     */
    public static function getGroups()
    {
        return [
            self::GROUP_ALL,
            self::GROUP_SCHOOL,
            self::GROUP_LUCEUM,
            self::GROUP_COLLEGE,
            self::GROUP_UNIVERSITY,
        ];
    }


}