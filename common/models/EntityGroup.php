<?php 

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Entity group
 * 
 * @property integer $entity_id
 * @property integer $group
 */
class EntityGroup extends ActiveRecord
{
    const GROUP_ALL = 'all';
    
    /**
     * @inheritod
     */
    public static function tableName()
    {
        return '{{%entity_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }
}