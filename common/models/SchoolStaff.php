<?php 

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $school_id
 * @property integer $user_id
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class SchoolStaff extends ActiveRecord
{
    const TYPE_STAFF = 1;
    const TYPE_STUDENT = 2;
    const TYPE_PARENT = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {       
        return '{{%school_staff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {   
        return [];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * Returns school staff type
     * @return array
     */
    public static function types() 
    {
        return [
            self::TYPE_PARENT,
            self::TYPE_STAFF,
            self::TYPE_STUDENT
        ];
    }
}   