<?php 

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * SchoolBase
 * 
 */
class School extends SchoolBase
{
    /**
     * Base region of region_id
     * @var integer
     */
    public $region_id;

    /**
     * @inhertidoc
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(), [
                [['region_id'], 'integer'],
                [['region_id'], 'compare', 'compareValue' => 1, 'operator' => '>=', 'message' => 'Необходимо выбрать регион'],
                [['city_id'], 'required', 'message' => 'Необходимо выбрать город или район'],
                [['type'], 'validateSchoolType'],
                [['city_id'], 'validateCity'],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'region_id' => 'Регион',
            'city_id' => 'Город или район',
            'type' => Yii::t('main', 'School Type'),
        ]);
    }

    /**
     * Validates selected city
     * @return void
     */
    public function validateCity()
    {
        if (isset($this->region_id) && isset($this->city_id)) {
            $cityExists = Entity::find()->where([
                'id' => $this->city_id, 
                'parent_id' => $this->region_id,
                'type' => Entity::TYPE_CITY
            ])->exists();

            if (!$cityExists) {
                $this->addError('city_id', 'Неправильный город или район');
            }
        }
    }

    /**
     * Declaring Relations
     * 
     */
    public function getCity()
    {
        return $this->hasOne(Entity::className(), ['id' => 'city_id']);
    }

    /**
     * Returns school type
     * @return array
     */
    public static function getSchoolTypes()
    {
        return [
            self::TYPE_SCHOOL => Yii::t('main', self::TYPE_SCHOOL),
            self::TYPE_LYCEUM => Yii::t('main', self::TYPE_LYCEUM),
            self::TYPE_COLLEGE => Yii::t('main', self::TYPE_COLLEGE),
            self::TYPE_UNIVERSITY => Yii::t('main', self::TYPE_UNIVERSITY)
        ];
    }

    /**
     * Validates school type
     * @return void
     */
    public function validateSchoolType()
    {
        if (isset($this->type) && !empty($this->type)) {
            if (!array_key_exists($this->type, $this->getSchoolTypes())){
                $this->addError('type', Yii::t('main', 'Wrong school type'));
            }
        }
    } 

    /**
     * Declaring Relations with SchoolStaff ar
     * 
     */
    public function getStaff()
    {
        return $this->hasMany(SchoolStaff::className(), ['school_id' => 'id']);    
    }

    /**
     * Declaring Relations with BankDetail ar
     * 
     */
    public function getBankDetails()
    {
        return $this->hasMany(BankDetail::className(), ['school_id' => 'id']);    
    }

    /**
     * Returns number of assigned users
     * @return integer
     */
    protected function countAssignedUsers($type = [])
    {
        return (new \yii\db\Query)
                ->from(['staff' => SchoolStaff::tableName()])
                ->leftJoin(['user' => User::tableName()], 'staff.user_id = user.id')
                ->where(['staff.school_id' => $this->id, 'user.status' => User::STATUS_ACTIVE, 'staff.type' => !empty($type) ? $type : SchoolStaff::types()])
                ->count();    
    }

    /**
     * Counts assigned staff
     * @return integer
     */
    public function countStaff()
    {
        return $this->countAssignedUsers(SchoolStaff::TYPE_STAFF);
    }

    /**
     * Counts assigned students
     * @return integer
     */
    public function countStudents()
    {
        return $this->countAssignedUsers(SchoolStaff::TYPE_STUDENT);
    }

    /**
     * Counts assigned student parents
     * @return integer
     */
    public function countParents()
    {
        return $this->countAssignedUsers(SchoolStaff::TYPE_PARENT);
    }
}

