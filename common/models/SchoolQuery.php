<?php

namespace common\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;

/**
 *
 */
class SchoolQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => NestedSetsQueryBehavior::className(),
            ],
        ];
    }
}