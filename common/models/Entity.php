<?php 

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Entity
 * 
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $parent_id
 * @property string $type
 * @property integer created_at
 * @property integer updated_at
 */
class Entity extends ActiveRecord
{
    const TYPE_REGION = 'region';
    const TYPE_CITY = 'city';
    const TYPE_MAJOR = 'major';

    /**
     * @inheritod
     */
    public static function tableName()
    {
        return '{{%entity}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'type'], 'string', 'max' => 30],
            [['parent_id'], 'integer'],
            [['name', 'type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'code' => 'Код',
        ];
    }


    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
}