<?php 

namespace common\models;

use creocoder\nestedsets\NestedSetsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * SchoolBase
 * 
 * @property integer $id
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $type
 * @property integer $status
 * @property string $name
 * @property string $number
 * @property integer $major
 * @property integer $city_id
 * @property string $address
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $website
 * @property string $inn
 * @property string $okonx
 * @property integer created_at
 * @property integer updated_at
 */
abstract class SchoolBase extends ActiveRecord
{
    const TYPE_SCHOOL = 'school';
    const TYPE_COLLEGE = 'college';
    const TYPE_LYCEUM = 'lyceum';
    const TYPE_UNIVERSITY = 'university';
    const TYPE_SCHOOL_CLASS = 'sgroup';
    const TYPE_COLLEGE_GROUP = 'cgroup';
    const TYPE_LYCEUM_GROUP = 'lgroup';

    const STATUS_ACTIVE = 1;
    const STATUS_INACITVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%school}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'number', 'address', 'phone', 'fax', 'inn', 'okonx', 'email', 'website'], 'trim'],
            [['name'], 'string', 'max' => 100],
            [['number', 'phone', 'fax', 'inn', 'okonx'], 'string', 'max' => 30],
            [['address'], 'string', 'max' => 500],
            [['email'], 'email'],
            [['website'], 'url', 'defaultScheme' => 'http'],
            [['major', 'city_id'], 'integer'],
            [['name'], 'validateName', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('main','Name'),
            'number' => \Yii::t('main','Number'),
            'email' => \Yii::t('main','Email'),
            'phone' => \Yii::t('main','Phone'),
            'fax' => \Yii::t('main','Fax'),
            'address' => \Yii::t('main','Address'),
            'website' => \Yii::t('main','website'),
            'inn' => \Yii::t('main','INN'),
            'okonx' => \Yii::t('main','Okonx'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new SchoolQuery(get_called_class());
    }

    /**
     * Validates selected major
     * @return void
     */
    public function validateMajor()
    {
        
    }

    /**
     * Validates name and number are empty
     * @return void
     */
    public function validateName()
    {
        if (empty($this->name) && empty($this->number)){
            $this->addError('name', 'Необходимо заполнить название или номер');
        }
    }
}