<?php 

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * Subjects
 */
class SubjectEntity extends Entity
{
    const TYPE_SUBJECT = 'subject';

    /**
     * Subject groups
     * @var array
     */
    public $groups;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['groups'], 'validateEntityGroup'],
            [['code', 'groups'], 'required'],
        ]);
    }

    /**
     * Validates subject group
     * @return void
     */
    public function validateEntityGroup()
    {
        if (isset($this->groups) && !empty($this->groups)) {
            $subjectGroups = SubjectEntityGroup::getGroups();
            foreach ($this->groups as $group) {
                if (!in_array($group, $subjectGroups)){
                    $this->addError('groups', \Yii::t('main', 'Wrong subject type'));
                    break;
                }
            }
        }
    }

}