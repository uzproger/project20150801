<?php 

namespace common\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * School bank detail
 * 
 * @property integer $id
 * @property string $name
 * @property string $treasury_name
 * @property string $number
 * @property string $mfo
 * @property string $inn
 * @property integer $is_treasury
 * @property integer $school_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class BankDetail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%school_bank_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'treasury_name'], 'string', 'max' => 60],
            [['number', 'mfo', 'inn'], 'string', 'max' => 30],
            [['name', 'treasury_name', 'number', 'mfo', 'inn'], 'trim'],
            [['school_id', 'is_treasury'], 'integer', 'min' => 0],
            [['name', 'number', 'mfo', 'school_id'], 'required'],
            [['treasury_name', 'inn'], 'required', 'when' => function($model){
                return $model->is_treasury == 1;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('main', 'Bank name'),
            'mfo' => \Yii::t('main', 'MFO'),
            'number' => \Yii::t('main', 'Bank account'),
            'treasury_name' => \Yii::t('main', 'Treasury name'),
            'inn' => \Yii::t('main', 'INN'),
            'created_at' => \Yii::t('main', 'Created'),
        ];
    }


}