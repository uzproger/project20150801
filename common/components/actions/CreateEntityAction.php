<?php 

namespace common\components\actions;

use yii\base\Action;

/**
 * 
 */
class CreateEntityAction extends Action
{
    /**
     * Model which extends \common\model\Entity
     * @var string
     */
    public $model;

    /**
     * View file
     * @var string
     */
    public $view;

    /**
     * @inhertidoc
     */
    public function run()
    {
        
    }
}