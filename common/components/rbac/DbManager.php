<?php 

namespace common\components\rbac;

use yii\rbac\Item;

/**
 * 
 */
class DbManager extends \yii\rbac\DbManager
{
    /**
     * Role group name
     * @var string
     */
    public $group = 'frontend';

    /**
     * Returns the roles that are assigned to the $this->group
     * @return yii\rbac\Role[]
     */
    public function getRoles()
    {
        if (empty($this->group)) {
            return [];
        }
        return $this->getRolesByGroup([$this->group]);
    }

    /**
     * Returns the roles that are assigned to the $group
     * @param  array $group 
     * @return yii\rbac\Role[]
     */
    public function getRolesByGroup($group = [])
    {
        $roles = parent::getRoles();
        if (empty($roles)){
            return [];
        }

        $groupRoles = [];
        foreach ($roles as $role) {
            if (isset($role->data['group']) && (empty($group) || in_array($role->data['group'], $group))) {
                $groupRoles[] = $role;
            }  
        }

        return $groupRoles;
    }
}