-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: davomatdb
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dv_entity`
--

DROP TABLE IF EXISTS `dv_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dv_entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dv_entity`
--

LOCK TABLES `dv_entity` WRITE;
/*!40000 ALTER TABLE `dv_entity` DISABLE KEYS */;
INSERT INTO `dv_entity` VALUES (1,'Ташкент г.',NULL,NULL,1,1430266689,1430321822),(2,'Мирзо Улугбекский район',NULL,1,2,1430266689,1430266689),(3,'Олмазарский район',NULL,1,2,1430266689,1430266689),(4,'Самаркандская обл.',NULL,0,1,1430321660,1430367574),(5,'Самарканд г.',NULL,4,2,1430321859,1430321859),(6,'Ташкентская обл.',NULL,0,1,1430367476,1430367553),(7,'Андижанская обл.',NULL,0,1,1430367595,1430367595),(8,'Бухарская обл.',NULL,0,1,1430367605,1430367605),(9,'Джизакская обл.',NULL,0,1,1430367613,1430367613),(10,'Кашкадарьинская обл.',NULL,0,1,1430367620,1430367620),(11,'Навоийская обл.',NULL,0,1,1430367626,1430367626),(12,'Наманганская обл.',NULL,0,1,1430367634,1430367634),(13,'Сурхандарьинская обл.',NULL,0,1,1430367642,1430367642),(14,'Сырдарьинская обл.',NULL,0,1,1430367648,1430367648),(15,'Ферганская обл.',NULL,0,1,1430367654,1430367654),(16,'Хорезмская обл.',NULL,0,1,1430367661,1430367661),(17,'Юнус Абадский район',NULL,1,2,1430367724,1430367724),(18,'Бектемирский район',NULL,1,2,1430367732,1430367732),(19,'Чиланзарский район',NULL,1,2,1430367742,1430367742),(20,'Яшнабадский район',NULL,1,2,1430367750,1430367750),(21,'Мирабадский район',NULL,1,2,1430367758,1430367758),(22,'Сергелийский район',NULL,1,2,1430367772,1430367772),(23,'Шайхантахурский район',NULL,1,2,1430367779,1430367779),(24,'Учтепинский район',NULL,1,2,1430367793,1430367793),(25,'Яккасарайский район',NULL,1,2,1430367803,1430367803),(26,'Акдарьинский',NULL,4,2,1430367840,1430367840),(27,'Булунгурский',NULL,4,2,1430367847,1430367847),(28,'Джамбайский',NULL,4,2,1430367852,1430367852),(29,'Иштыханский',NULL,4,2,1430367858,1430367858),(30,'Каттакурганский',NULL,4,2,1430367865,1430367865),(31,'Кошрабадский',NULL,4,2,1430367871,1430367871),(32,'Нарпайский',NULL,4,2,1430367876,1430367876),(33,'Нурабадский',NULL,4,2,1430367882,1430367882),(34,'Пайарыкский',NULL,4,2,1430367888,1430367888),(35,'Пастдаргомский',NULL,4,2,1430367894,1430367894),(36,'Пахтачийский',NULL,4,2,1430367901,1430367901),(37,'Тайлакский',NULL,4,2,1430367911,1430367911),(38,'Ургутский',NULL,4,2,1430367917,1430367917),(39,'Зангиатинский',NULL,6,2,1430367939,1430367939),(40,'Аккурганский',NULL,6,2,1430367946,1430367946),(41,'Ахангаранский',NULL,6,2,1430367952,1430367952),(42,'Бекабадский',NULL,6,2,1430367958,1430367958),(43,'Бостанлыкский',NULL,6,2,1430367964,1430367964),(44,'Букинский',NULL,6,2,1430367971,1430367971),(45,'Кибрайский',NULL,6,2,1430367977,1430367977),(46,'Куйичирчикский',NULL,6,2,1430367983,1430367983),(47,'Паркентский',NULL,6,2,1430367988,1430367988),(48,'Пскентский',NULL,6,2,1430367994,1430367994),(49,'Уртачирчикский',NULL,6,2,1430368000,1430368000),(50,'Чиназский',NULL,6,2,1430368006,1430368006),(51,'Юкоричирчикский',NULL,6,2,1430368012,1430368012),(52,'Янгиюльский',NULL,6,2,1430368019,1430368019),(53,'Алтынкульский',NULL,7,2,1430368040,1430368040),(54,'Андижанский',NULL,7,2,1430368046,1430368046),(55,'Асакинский',NULL,7,2,1430368053,1430368053),(56,'Балыкчинский',NULL,7,2,1430368060,1430368060),(57,'Бозский',NULL,7,2,1430368067,1430368067),(58,'Булакбашинский',NULL,7,2,1430368073,1430368073),(59,'Джалакудукский',NULL,7,2,1430368079,1430368079),(60,'Избасканский',NULL,7,2,1430368085,1430368085),(61,'Кургантепинский',NULL,7,2,1430368092,1430368092),(62,'Мархаматский',NULL,7,2,1430368097,1430368097),(63,'Пахтаабадский',NULL,7,2,1430368104,1430368104),(64,'Улугнорский',NULL,7,2,1430368109,1430368109),(65,'Ходжаабадский',NULL,7,2,1430368116,1430368116),(66,'Шахриханский',NULL,7,2,1430368122,1430368122),(67,'Алатский',NULL,8,2,1430368137,1430368137),(68,'Бухарский',NULL,8,2,1430368142,1430368142),(69,'Вабкентский',NULL,8,2,1430368147,1430368147),(70,'Гиждуванский',NULL,8,2,1430368152,1430368152),(71,'Жондорский',NULL,8,2,1430368159,1430368159),(72,'Каганский',NULL,8,2,1430368165,1430368165),(73,'Каракульский',NULL,8,2,1430368172,1430368172),(74,'Караулбазарский',NULL,8,2,1430368177,1430368177),(75,'Пешкунский',NULL,8,2,1430368182,1430368182),(76,'Ромитанский',NULL,8,2,1430368188,1430368188),(77,'Шафирканский',NULL,8,2,1430368194,1430368194),(78,'Арнасайский',NULL,9,2,1430368209,1430368220),(79,'Бахмальский',NULL,9,2,1430368226,1430368226),(80,'Галляаральский',NULL,9,2,1430368237,1430368237),(81,'Джизакский',NULL,9,2,1430368244,1430368244),(82,'Дустликский',NULL,9,2,1430368250,1430368250),(83,'Зааминский',NULL,9,2,1430368256,1430368256),(84,'Зарбдарский',NULL,9,2,1430368263,1430368263),(85,'Зафарабадский',NULL,9,2,1430368270,1430368270),(86,'Мирзачульский',NULL,9,2,1430368276,1430368276),(87,'Пахтакорский',NULL,9,2,1430368280,1430368280),(88,'Фаришский',NULL,9,2,1430368286,1430368286),(89,'Янгиабадский',NULL,9,2,1430368292,1430368292),(90,'Гузарский',NULL,10,2,1430368319,1430368319),(91,'Дехканабадский',NULL,10,2,1430368328,1430368328),(92,'Камашинский',NULL,10,2,1430368339,1430368339),(93,'Каршинский',NULL,10,2,1430368348,1430368348),(94,'Касанский',NULL,10,2,1430368358,1430368358),(95,'Касбийский',NULL,10,2,1430368367,1430368367),(96,'Китабский',NULL,10,2,1430368374,1430368374),(97,'Миришкорский',NULL,10,2,1430368381,1430368381),(98,'Мубарекский',NULL,10,2,1430368390,1430368390),(99,'Нишанский',NULL,10,2,1430368398,1430368398),(100,'Чиракчинский',NULL,10,2,1430368405,1430368405),(101,'Шахрисабзский',NULL,10,2,1430368414,1430368414),(102,'Яккабагский',NULL,10,2,1430368423,1430368423),(103,'Канимехский',NULL,11,2,1430368441,1430368441),(104,'Кызылтепинский',NULL,11,2,1430368447,1430368447),(105,'Навбахорский',NULL,11,2,1430368455,1430368455),(106,'Карманинский',NULL,11,2,1430368461,1430368461),(107,'Нуратинский',NULL,11,2,1430368466,1430368466),(108,'Тамдынский',NULL,11,2,1430368472,1430368472),(109,'Учкудукский',NULL,11,2,1430368479,1430368479),(110,'Хатырчинский',NULL,11,2,1430368484,1430368484),(111,'Касансайский',NULL,12,2,1430368512,1430368512),(112,'Мингбулакский',NULL,12,2,1430368517,1430368517),(113,'Наманганский',NULL,12,2,1430368522,1430368522),(114,'Нарынский',NULL,12,2,1430368528,1430368528),(115,'Папский',NULL,12,2,1430368533,1430368533),(116,'Туракурганский',NULL,12,2,1430368542,1430368542),(117,'Уйчинский',NULL,12,2,1430368547,1430368547),(118,'Учкурганский',NULL,12,2,1430368554,1430368554),(119,'Чартакский',NULL,12,2,1430368560,1430368560),(120,'Чустский',NULL,12,2,1430368566,1430368566),(121,'Янгикурганский',NULL,12,2,1430368573,1430368573),(122,'Алтынсайский',NULL,13,2,1430368598,1430368598),(123,'Ангорский',NULL,13,2,1430368607,1430368607),(124,'Байсунский',NULL,13,2,1430368612,1430368612),(125,'Денауский',NULL,13,2,1430368617,1430368617),(126,'Джаркурганский',NULL,13,2,1430368623,1430368623),(127,'Кизирикский',NULL,13,2,1430368628,1430368628),(128,'Кумкурганский',NULL,13,2,1430368633,1430368633),(129,'Музрабадский',NULL,13,2,1430368639,1430368639),(130,'Сариасийский',NULL,13,2,1430368644,1430368644),(131,'Термезский',NULL,13,2,1430368651,1430368651),(132,'Узунский',NULL,13,2,1430368660,1430368660),(133,'Шерабадский',NULL,13,2,1430368669,1430368669),(134,'Шурчинский',NULL,13,2,1430368675,1430368675),(135,'Акалтынский',NULL,14,2,1430368692,1430368692),(136,'Баяутский',NULL,14,2,1430368698,1430368698),(137,'Гулистанский',NULL,14,2,1430368703,1430368703),(138,'Мирзаабадский',NULL,14,2,1430368708,1430368708),(139,'Сайхунабадский',NULL,14,2,1430368713,1430368713),(140,'Сардобский',NULL,14,2,1430368718,1430368718),(141,'Сырдарьинский',NULL,14,2,1430368724,1430368724),(142,'Хавастский',NULL,14,2,1430368729,1430368729),(143,'Алтыарыкский',NULL,15,2,1430368790,1430368790),(144,'Багдадский',NULL,15,2,1430369005,1430369005),(145,'Бешарыкский',NULL,15,2,1430369010,1430369010),(146,'Бувайдинский',NULL,15,2,1430369018,1430369018),(147,'Дангаринский',NULL,15,2,1430369025,1430369025),(148,'Кувинский',NULL,15,2,1430369032,1430369032),(149,'Куштепинский',NULL,15,2,1430369037,1430369037),(150,'Риштанский',NULL,15,2,1430369043,1430369043),(151,'Сохский',NULL,15,2,1430369049,1430369049),(152,'Ташлакский',NULL,15,2,1430369055,1430369055),(153,'Узбекистанский',NULL,15,2,1430369060,1430369060),(154,'Учкуприкский',NULL,15,2,1430369067,1430369067),(155,'Ферганский',NULL,15,2,1430369072,1430369072),(156,'Фуркатский',NULL,15,2,1430369077,1430369077),(157,'Язъяванский',NULL,15,2,1430369086,1430369086),(158,'Ургенч',NULL,16,2,1430369107,1430369107),(159,'Багатский',NULL,16,2,1430369116,1430369116),(160,'Гурленский',NULL,16,2,1430369124,1430369124),(161,'Кошкупырский',NULL,16,2,1430369132,1430369132),(162,'Ургенчский',NULL,16,2,1430369140,1430369140),(163,'Хазараспский',NULL,16,2,1430369149,1430369149),(164,'Ханкинский',NULL,16,2,1430369157,1430369157),(165,'Хивинский',NULL,16,2,1430369167,1430369167),(166,'Шаватский',NULL,16,2,1430369176,1430369176),(167,'Янгибазарский',NULL,16,2,1430369184,1430369184),(168,'Янгиарыкский',NULL,16,2,1430369192,1430369192);
/*!40000 ALTER TABLE `dv_entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dv_major`
--

DROP TABLE IF EXISTS `dv_major`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dv_major` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dv_major`
--

LOCK TABLES `dv_major` WRITE;
/*!40000 ALTER TABLE `dv_major` DISABLE KEYS */;
/*!40000 ALTER TABLE `dv_major` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dv_migration`
--

DROP TABLE IF EXISTS `dv_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dv_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dv_migration`
--

LOCK TABLES `dv_migration` WRITE;
/*!40000 ALTER TABLE `dv_migration` DISABLE KEYS */;
INSERT INTO `dv_migration` VALUES ('m000000_000000_base',1430266681),('m130524_201442_init',1430266689),('m130524_201443_major',1430266689),('m130524_201444_entity',1430266689),('m150424_140352_school',1430266689),('m150424_140441_school_staff',1430266689),('m150425_125511_school_bank_detail',1430266689);
/*!40000 ALTER TABLE `dv_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dv_school`
--

DROP TABLE IF EXISTS `dv_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dv_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tree` int(11) unsigned NOT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `depth` int(11) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `name` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `number` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `major` smallint(11) unsigned DEFAULT NULL,
  `city_id` smallint(11) unsigned DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `phone` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inn` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `okonx` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `major_idx` (`major`),
  KEY `city_idx` (`city_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dv_school`
--

LOCK TABLES `dv_school` WRITE;
/*!40000 ALTER TABLE `dv_school` DISABLE KEYS */;
INSERT INTO `dv_school` VALUES (1,1,1,2,0,1,0,'','203',NULL,24,'ul.Qurilish','','','','123456789','','',1430499215,1430499215);
/*!40000 ALTER TABLE `dv_school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dv_school_bank_detail`
--

DROP TABLE IF EXISTS `dv_school_bank_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dv_school_bank_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `treasury_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `mfo` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `inn` char(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_treasury` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `school_id` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `school_idx` (`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dv_school_bank_detail`
--

LOCK TABLES `dv_school_bank_detail` WRITE;
/*!40000 ALTER TABLE `dv_school_bank_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `dv_school_bank_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dv_school_staff`
--

DROP TABLE IF EXISTS `dv_school_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dv_school_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_unq` (`school_id`,`user_id`,`type`),
  KEY `user_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dv_school_staff`
--

LOCK TABLES `dv_school_staff` WRITE;
/*!40000 ALTER TABLE `dv_school_staff` DISABLE KEYS */;
/*!40000 ALTER TABLE `dv_school_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dv_user`
--

DROP TABLE IF EXISTS `dv_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dv_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dv_user`
--

LOCK TABLES `dv_user` WRITE;
/*!40000 ALTER TABLE `dv_user` DISABLE KEYS */;
INSERT INTO `dv_user` VALUES (1,'admin','','$2y$13$ugmoR53rX3eiKYgURifnWOJQdHFOopZZ37VUioAeTTl2AwkExWHZO',NULL,'admin@admin.com',10,1,1430266687,1430266687),(2,'user','','$2y$13$xP9CW7k4sODiX/VEVboLYuFsZGAq1M9K83f82/TaOR3Gj/BFsBjWS',NULL,'user@admin.com',10,0,1430266689,1430266689);
/*!40000 ALTER TABLE `dv_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-02  4:02:52
