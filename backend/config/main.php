<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_backendUser', // unique for backend
            ]
        ],
        'authManager' => [
            'class' => 'common\components\rbac\DbManager',
            'group' => 'backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<type:(schools|lyceums|colleges|universities)>' => '/school/index',                
                '<type:(school|lyceum|college|university)>/create' => '/school/create',
                '<type:(school|lyceum|college|university)>/<id:\d+>/update' => '/school/update',
                '<type:(school|lyceum|college|university)>/<id:\d+>' => '/school/view',
                '<school_type:(school|lyceum|college|university)>/<school_id:\d+>/<controller:(bank|staff)>' => '/<controller>/index',
                '<school_type:(school|lyceum|college|university)>/<school_id:\d+>/<controller:(bank|staff)>/add' => '/<controller>/create',
                '<school_type:(school|lyceum|college|university)>/<school_id:\d+>/<controller:(bank|staff)>/<id:\d+>/update' => '/<controller>/update',
                '<school_type:(school|lyceum|college|university)>/<school_id:\d+>/<controller:(bank|staff)>/<id:\d+>/delete' => '/<controller>/delete',
                '<type:(regions)>' => '/region/index',
                '<region:\d+>/<type:(cities)>' => '/region/index',
                '<type:(region)>/create' => '/region/create',
                '<region:\d+>/<type:(city)>/create' => '/region/create',
                '<type:(region|city)>/<id:\d+>/update' => '/region/update',

                // 'majors' => '/entity/major/list',
                // 'major/create' => '/entity/major/create',
                // 'major/<id:\d>/update' => '/entity/major/update',

            ],
        ],
    ],
    'params' => $params,
];
