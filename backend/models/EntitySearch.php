<?php 

namespace backend\models;

use common\components\helpers\WordHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * 
 */
class EntitySearch extends Model
{
    /**
     * Entity type
     * @var string
     */
    public $type;

    /**
     * Entity parent id
     * @var string
     */
    public $parent_id;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init(); 
        $this->type = WordHelper::singularize($this->type);
    }
    
    /**
     * Search school by request criteria
     * @param  array|null $params Filter params
     * @return ActiveDataProvider Data provider with schools
     */
    public function search($params = [])
    {
        $query = (new \yii\db\Query())
                    ->select('t1.*')
                    ->from(['t1' => Entity::tableName()])
                    ->where(['t1.type' => $this->type]);

        if (isset($this->parent_id) && $this->parent_id > 0) {
            $query->andWhere(['t1.parent_id' => $this->parent_id]);
        } else {
            $query->leftJoin(['t2' => Entity::tableName()], 't1.id = t2.parent_id');
            $query->addSelect('COUNT(t2.id) childs');
            $query->groupBy(['t1.id']);
        }
        $query->orderBy('t1.name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30
            ]
        ]);

        return $dataProvider;
    }
}