<?php 

namespace backend\models;

use common\components\helpers\WordHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * 
 */
class SchoolSearch extends Model
{
    /**
     * School type
     * @var integer
     */
    public $type;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init(); 
        $this->type = WordHelper::singularize($this->type);
    }

    /**
     * Search school by request criteria
     * @param  array|null $params Filter params
     * @return ActiveDataProvider Data provider with schools
     */
    public function search($params = [])
    {
        $query = (new \yii\db\Query())
                ->select('t1.*, t2.name as city, t3.name as region')
                ->from(['t1' => School::tableName()])
                ->where(['t1.lft' => 1, 't1.status' => School::STATUS_ACTIVE])
                ->leftJoin(['t2' => Region::tableName()], 't2.id = t1.city_id')
                ->leftJoin(['t3' => Region::tableName()], 't3.id = t2.parent_id')
                ->orderBy('t1.number, t1.name');

        

        if (isset($this->type) && $this->type != ''){
            $query->andWhere(['t1.type' => $this->type]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30
            ]
        ]);

        return $dataProvider;
    }
}