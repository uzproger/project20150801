<?php 

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * 
 */
class BankDetailSearch extends Model
{
    /**
     * School id
     * @var integer
     */
    public $school_id;

    /**
     * For treasure bank value should be 1
     * @var integer
     */
    public $is_treasury = 0;

   /**
     * Search school by request criteria
     * @param  array|null $params Filter params
     * @return ActiveDataProvider Data provider with schools
     */
    public function search($params = [])
    {
        $query = BankDetail::find()->where([
            'school_id' => $this->school_id,
            'is_treasury' => $this->is_treasury
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30
            ]
        ]);

        return $dataProvider;
    }
}