<?php
namespace backend\models;

use common\models\User;

/**
 * Login form
 */
class LoginForm extends \common\models\LoginForm
{
	/**
     * @inheritdoc
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $user = User::findByUsername($this->username);
            $auth = \Yii::$app->getAuthManager();
            if ($user && $auth->checkAccess($user->id, 'backendLogin')) {
                $this->_user = $user;  
            }
        }

        return $this->_user;
    }
}