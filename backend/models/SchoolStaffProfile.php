<?php
namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * School staff
 *
 * @property integer $school_id
 * @property string $fistname
 * @property string $lastname
 * @property string $middlename
 * @property string $mobile
 * @property string $email
 * @property array $roles
 */
class SchoolStaffProfile extends \common\models\User
{
    /**
     * Roles those not allowed for multiple assignment
     * @var array
     */
    private $oneTimeAssignmentRoles = [
        'director', 
        'headmaster'
    ];

    /** 
     * School id
     * @var integer
     */
    public $school_id;

    /** 
     * Selected roles
     * @var array
     */
    public $roles = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['mobile', 'status'], 'unique', 'targetAttribute' => ['mobile', 'status'], 'message' => Yii::t('main', 'Phone "{value}" has already been taken')],
            [['school_id', 'roles', 'mobile'], 'required'],
            [['roles'], 'validateRoles'],
            [['roles'], 'validateNotAllowedRoles'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'roles' => Yii::t('main', 'Roles'),
        ]);
    }

    /**
     * Returns school roles
     * @return array
     */
    public function getSchoolRoles()
    {
        $data = Yii::$app->authManager->getRolesByGroup(['frontend']);
        $roles = [];
        foreach ($data as $role) {
            $roles[$role->name] = Yii::t('main', $role->name);
        }
        return $roles;
    }

    /**
     * Validates selected roles
     * @return void
     */
    public function validateRoles()
    {
        if (isset($this->roles) && !empty($this->roles)){
            $predifinedRoles = Yii::$app->authManager->getRolesByGroup(['frontend']);
            $predifinedRoles = ArrayHelper::map($predifinedRoles, 'name', 'name');
            foreach ($this->roles as $roleName) {
                if (!array_key_exists($roleName, $predifinedRoles)){
                    $this->addError('roles', Yii::t('main', 'Wrong role'));
                    break;
                }
            }
        }
    }

    /**
     * Validates for not allowed roles
     * @return void
     */
    public function validateNotAllowedRoles()
    {
        if (isset($this->roles) && !empty($this->roles)){
            $notAllowedRoles = $this->getNotAllowedRoles();
            if (!empty($notAllowedRoles)) {
                foreach ($this->roles as $roleName) {
                    if (array_key_exists($roleName, $notAllowedRoles)){
                        $this->addError('roles', Yii::t('main', 'This role already assigned'));
                        break;
                    }
                }
            }
        }
    }

    /**
     * Creates new school staff
     * @return boolean
     */
    public function create()
    {
        if ($this->validate()){
            $transaction = $this->db->beginTransaction();
            try {
                $auth = Yii::$app->getAuthManager();
                $this->generateUsernameByMobile();
                $this->generateRandomPassword();
                if ($this->save(false)){
                    foreach ($this->roles as $roleName) {
                        $role = $auth->getRole($roleName);
                        if (!$auth->assign($role, $this->id)){
                            throw new \Exception("Cannot assign this user to selected role");
                        }
                    }

                    $schoolStaff = new SchoolStaff(['school_id' => $this->school_id]);
                    $schoolStaff->user_id = $this->id;
                    $schoolStaff->type = SchoolStaff::TYPE_STAFF;
                    if (!$schoolStaff->save()){
                        throw new \Exception("Cannot assign this user to selected school");
                    }

                    $transaction->commit();
                    return true;
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
            }
        }

        return false;
    }

    /**
     * Updates school staff profile
     * @return boolean
     */
    public function updateProfile()
    {
        if ($this->validate()){
            $transaction = $this->db->beginTransaction();
            try {
                $auth = Yii::$app->getAuthManager();
                $this->generateUsernameByMobile();
                if ($this->save(false)){

                    if (!$auth->revokeAll($this->id)) {
                        throw new \Exception("Cannot revoke roles of this user");
                    }

                    foreach ($this->roles as $roleName) {
                        $role = $auth->getRole($roleName);
                        if (!$auth->assign($role, $this->id)){
                            throw new \Exception("Cannot assign this user to selected role");
                        }
                    }

                    $transaction->commit();
                    return true;
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
            }
        }

        return false;
    }

    /**
     * Deletes school staff profile
     * @return boolean
     */
    public function deleteProfile()
    {
        $transaction = $this->db->beginTransaction();
        try {
            $auth = Yii::$app->getAuthManager();
            if (!$auth->revokeAll($this->id)) {
                throw new \Exception("Cannot revoke roles of this user");
            }
            $this->status = self::STATUS_DELETED;
            if ($this->save(false)) {
                $transaction->commit();
                return true;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
        }
        return false;
    }


    /**
     * Returns not allowed roles for new adding staff
     * @return array
     */
    public function getNotAllowedRoles()
    {
        $result = [];
        if (isset($this->school_id) && !empty($this->school_id)){
            foreach ($this->oneTimeAssignmentRoles as $role) {
                $assignedRole = $this->getUserByAssignedRole($role);
                if ($assignedRole && $assignedRole['user_id'] != $this->id) {
                    $result[] = $role;
                }
            }
        }
        return $result;
    }

    /**
     * Return given role name already assined in this school staff
     * @param  string $roleName
     * @return integer
     */
    private function getUserByAssignedRole($roleName)
    {
        return (new \yii\db\Query)
                ->select('staff.user_id')
                ->from(['staff' => SchoolStaff::tableName()])
                ->leftJoin(['assignment' => \Yii::$app->authManager->assignmentTable], 'staff.user_id = assignment.user_id')
                ->leftJoin(['user' => User::tableName()], 'staff.user_id = user.id')
                ->where(['staff.school_id' => $this->school_id, 'assignment.item_name' => $roleName, 'user.status' => User::STATUS_ACTIVE])
                ->one();
    }
}