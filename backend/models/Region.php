<?php 
namespace backend\models;


class Region extends \common\models\Entity
{
    /**
     * Return region list for dropdown list
     * @return array
     */
    public static function getRegions()
    {
        $query = self::find()
                ->select('id, name')
                ->where(['type' => self::TYPE_REGION])
                ->orderBy('name')
                ->asArray();

        return $query->all();
    }
}