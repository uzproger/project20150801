<?php 

namespace backend\models;

use backend\models\SchoolStaff;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * 
 */
class SchoolStaffSearch extends Model
{
    /**
     * School id
     * @var integer
     */
    public $school_id;

    /**
     * Search school by request criteria
     * @param  array|null $params Filter params
     * @return ActiveDataProvider Data provider with schools
     */
    public function search($params = [])
    {
        $auth = \Yii::$app->authManager;

        $query = (new Query)
            ->select('staff.*, user.firstname, user.lastname, user.middlename, user.mobile, GROUP_CONCAT(role.name) AS roles')
            ->from(['staff' => SchoolStaff::tableName()])
            ->where(['staff.school_id' => $this->school_id, 'user.status' => User::STATUS_ACTIVE, 'staff.type' => SchoolStaff::TYPE_STAFF])
            ->leftJoin(['user' => User::tableName()], 'user.id = staff.user_id')
            ->leftJoin(['assignment' => $auth->assignmentTable], 'user.id = assignment.user_id')
            ->leftJoin(['role' => $auth->itemTable], 'assignment.item_name = role.name')
            ->groupBy(['school_id', 'user_id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Search one school staff
     * @param  array $params
     * @return null|yii\db\ActiveRecord
     */
    public function searchOne($params = [])
    {
        $auth = \Yii::$app->authManager;

        $model = SchoolStaffProfile::find()
                ->select('user.*, staff.school_id, GROUP_CONCAT(role.name) AS roles')
                ->from(['user' => SchoolStaffProfile::tableName()])
                ->leftJoin(['staff' => SchoolStaff::tableName()], 'staff.user_id = user.id')
                ->leftJoin(['assignment' => $auth->assignmentTable], 'user.id = assignment.user_id')
                ->leftJoin(['role' => $auth->itemTable], 'assignment.item_name = role.name')
                ->where(['user.id' => $params['id'], 'staff.school_id' => $this->school_id])
                ->groupBy(['staff.school_id', 'user.id'])
                ->one();

        if ($model) {
            $model->roles = explode(',', $model->roles);
        }

        return $model;
    }
}