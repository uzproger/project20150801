<?php 

namespace backend\models;

use yii\helpers\ArrayHelper;

/**
 * 
 */
class School extends \common\models\School
{
    /**
     * Return formated school name
     * @return string
     */
    public function getTitle()
    {
        $title = [];

        if ($this->number != ''){
            $title[] = '№ '.$this->number;
        }

        if ($this->name != '') {
            $title[] = $this->name;
        }

        return \Yii::t('main', $this->type).(!empty($title) ? ': '.implode($title, ', ') : '');
    }

    /**
     * Returns number of schools by type
     * @return array
     */
    public static function countSchoolsByType()
    {
        $query = (new \yii\db\Query())
            ->select('type, COUNT(*) as num')
            ->from(self::tableName())
            ->where(['type' => array_keys(self::getSchoolTypes())])
            ->groupBy('type')->all();

        $data['total'] = 0;
        foreach ($query as $row) {
            $data[$row['type']] = $row['num'];
            $data['total'] = $data['total'] + $data[$row['type']];
        }

        return $data;
    }
}