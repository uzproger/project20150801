$(function() {
	function initRegion()
	{
		var selectedRegion = $('#regions').val();
		if (selectedRegion > 0) {
			$.post(
					'/region/cities', 
					{'region': selectedRegion})
					.done(function(data){
						if (data != null) {
							$('#city').empty().append(function(){
								var option = '';
								$.each(eval(data.replace(/[\r\n]/, "")), function(key, value) {
									option += '<option value="'+value.id+'"'
									if ($("#selectedCity").val() > 0 && $("#selectedCity").val() == value.id) {
										option += ' selected';
									} 
									option += '>'+ value.name + '</option>';
								});
								if (option != '') {
									$('#city').removeAttr('disabled');
								}
								return option;
							});
						}
					});
		} else {
			$('#city').empty().attr('disabled', 'true');
			$("#selectedCity").val(0);
		}
	}
	
	$('#regions').change(function(){
		initRegion();
	});
	
	$('#city').change(function(){
		$("#selectedCity").val($('#city').val());
	})
	initRegion();
})