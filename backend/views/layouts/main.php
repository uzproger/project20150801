<?php

use backend\assets\AppAsset;
use backend\components\widgets\TabMenuWidget;
use backend\models\School;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php

            $schoolCount = School::countSchoolsByType();

            NavBar::begin([
                'brandLabel' => 'Дневник (backend)',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Главная', 'url' => ['/site/index']],
                ['label' => 'Учреждение ('.(isset($schoolCount['total']) ? $schoolCount['total'] : 0).')', 'url' => '#', 
                    'items' => [
                        ['label' => 'Школы ('.(isset($schoolCount[School::TYPE_SCHOOL]) ? $schoolCount[School::TYPE_SCHOOL] : 0).')', 'url' => ['/schools']],
                        ['label' => 'Лицеи ('.(isset($schoolCount[School::TYPE_LYCEUM]) ? $schoolCount[School::TYPE_LYCEUM] : 0).')', 'url' => ['/lyceums']],
                        ['label' => 'Колледжи ('.(isset($schoolCount[School::TYPE_COLLEGE]) ? $schoolCount[School::TYPE_COLLEGE] : 0).')', 'url' => ['/colleges']],
                        ['label' => 'Университеты ('.(isset($schoolCount[School::TYPE_UNIVERSITY]) ? $schoolCount[School::TYPE_UNIVERSITY] : 0).')', 'url' => ['/universities']],
                    ]
                ],
                ['label' => 'Настройки', 'url' => '#',
                    'items' => [
                        ['label' => 'Предметы', 'url' => ['subject/index']],
                        ['label' => 'Регионы', 'url' => ['region/index', 'type' => 'regions']],  
                        ['label' => 'Специальности', 'url' => '#'],  
                    ]
                ],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="site-about">
                <h3>
                    <?= Html::encode($this->title) ?>
                    <?php if (isset($this->params['top_buttons']) && !empty($this->params['top_buttons'])) { ?>
                        <?php foreach ($this->params['top_buttons'] as $button) { ?>
                            <?= Html::a($button['label'], $button['url'], $button['options']) ?>
                        <?php }?>
                    <?php } ?>
                </h3>
                <?= TabMenuWidget::widget([
                        'menus' => isset($this->params['tabs']) ? $this->params['tabs'] : []
                ])?>
                <?= $content ?>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; Dnevnik <?= date('Y') ?></p>
        <p class="pull-right"></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
