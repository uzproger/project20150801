<?php

use backend\models\Entity;
use common\components\helpers\WordHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = ($model->type == Entity::TYPE_CITY ? $region->name.': ' : '').($model->isNewRecord ? \Yii::t('main','New '.$model->type) : $model->name );

?>
<p>&nbsp;</p>
<?php $form = ActiveForm::begin([
    'id' => 'entity-form', 
    'layout' => 'horizontal',
    'enableClientScript' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-6',
        ],
    ],
]); ?>
<?= $form->field($model, 'name') ?>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::a('Отменить', ($model->type == Entity::TYPE_CITY ? ['region/index', 'type' => WordHelper::pluralize($model->type), 'region' => $region->id] : ['region/index', 'type' => WordHelper::pluralize($model->type)]), ['class' => 'btn btn-default', 'name' => 'cancel-button']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>