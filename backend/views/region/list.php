<?php

use backend\models\Entity;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


$this->title = $type == 'cities' ? $region->name : \Yii::t('main', $type);

$this->params['top_buttons'][] = [
    'label' => 'Добавить',
    'url' => ($type == 'cities' ? ['region/create', 'type' => $searchModel->type, 'region' => $region->id] : ['region/create', 'type' => $searchModel->type]),
    'options' => ['class' => 'btn btn-success btn-sm pull-right']
];

if ($type == 'cities') {
    $this->params['top_buttons'][] = [
            'label' => 'Все регионы',
            'url' => ['region/index', 'type' => 'regions'],
            'options' => ['class' => 'btn btn-default btn-sm pull-right', 'style' => 'margin-right: 5px;']
    ];
}

echo GridView::widget(
    [
        'id' => 'entity-grid',
        'summary' => '',
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => \Yii::t('main', 'Name'),
                'format' => 'html',
                'value' => function($model) {
                    return ($model['type'] == Entity::TYPE_REGION) ? Html::a($model['name'], ['region/index', 'region' => $model['id'], 'type' => 'cities']) : $model['name'];
                }
            ],
            [
                'attribute' => 'childs',
                'label' => \Yii::t('main', 'Cities num'),
                'visible' => ($type == 'regions') ? true : false,
            ],
            [
                'attribute' => 'created_at',
                'label' => \Yii::t('main', 'created_at'),
                'format' => ['date', 'php:Y-m-d H:i:s']
            ],
            [
                'attribute' => 'updated_at',
                'label' => \Yii::t('main', 'updated_at'),
                'format' => ['date', 'php:Y-m-d H:i:s']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['region/update', 'id' => $model['id'], 'type' => $model['type']]);
                    },
                    'delete' => function($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#');
                    }
                ],
            ],

        ]
    ]
);
?>