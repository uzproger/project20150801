<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->render('//school/menu', ['school' => $school, 'current' => 'bank']);

?>
<p>&nbsp;</p>
<?php $form = ActiveForm::begin([
    'id' => 'school-form', 
    'layout' => 'horizontal',
    'enableClientScript' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-6',
        ],
    ],
]); ?>
<?php if ($model->is_treasury == 1) { ?>
<?= $form->field($model, 'treasury_name') ?>
<?php  } ?>
<?= $form->field($model, 'number') ?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'mfo') ?>
<?php if ($model->is_treasury == 1) { ?>
<?= $form->field($model, 'inn') ?>
<?php  } ?>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::a('Отменить', ['bank/index', 'school_type' => $school->type, 'school_id' => $school->id], ['class' => 'btn btn-default', 'name' => 'cancel-button']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>