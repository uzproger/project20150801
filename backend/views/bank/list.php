<?php 

/* School bank details */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->render('//school/menu', ['school' => $school, 'current' => 'bank']);
?>
<p>&nbsp;</p>
<h4>
    Банковские реквизиты
    <?= Html::a('Добавить', ['bank/create', 'school_id' => $school->id, 'school_type' => $school->type], ['class' => 'btn btn-success btn-xs pull-right'])?>
</h4>
<?php
Pjax::begin();
echo GridView::widget(
    [
        'id' => 'bank-account-grid',
        'summary' => '',
        'sorter' => [],
        'dataProvider' => $schoolBankProvider,
        'columns' => [
            'name',
            'number',
            'mfo',
            [
                'attribute' => 'inn',
                'label' => \Yii::t('main','INN'),
                'value' => function($model) use ($school) {
                    return $school->inn;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model) use ($school) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['bank/update', 'school_type' => $school->type, 'school_id' => $school->id, 'id' => $model->id]);
                    },
                    'delete' => function($url, $model) use ($school) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#');
                    }
                ],
            ],
        ]
    ]
);
Pjax::end();
?>
<p>&nbsp;</p>
<h4>
    Банковские реквизиты казначейство
     <?= Html::a('Добавить', ['bank/create', 'school_id' => $school->id, 'school_type' => $school->type, 'treasury' => 1], ['class' => 'btn btn-success btn-xs pull-right'])?>
</h4>
<?php
Pjax::begin();
echo GridView::widget(
    [
        'id' => 'bank-account-grid',
        'summary' => '',
        'dataProvider' => $treasuryBankProvider,
        'columns' => [
            'treasury_name',
            'name',
            'number',
            'mfo',
            'inn',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model) use ($school) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['bank/update', 'school_type' => $school->type, 'school_id' => $school->id, 'id' => $model->id]);
                    },
                    'delete' => function($url, $model) use ($school) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#');
                    }
                ],
            ],
        ]
    ]
);
Pjax::end();
?>