<?php 

/* School staff */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->render('//school/menu', ['school' => $school, 'current' => 'staff']);
?>
<p>&nbsp;</p>
<h4>
    Все сотрудники
    <?= Html::a('Добавить', ['staff/create', 'school_id' => $school->id, 'school_type' => $school->type], ['class' => 'btn btn-success btn-xs pull-right'])?>
</h4>
<?php
Pjax::begin();
echo GridView::widget(
    [
        'id' => 'bank-account-grid',
        'summary' => '',
        'sorter' => [],
        'dataProvider' => $schoolStaffProvider,
        'columns' => [
            [
                'label' => \Yii::t('main','FIO'),
                'value' => function($model){
                    return $model['lastname'].' '.$model['firstname'].' '.$model['middlename'];
                }
            ],
            [
                'label' => \Yii::t('main','Roles'),
                'value' => function($model){
                    $roles = explode(",", $model['roles']);
                    $str = [];
                    foreach ($roles as $role) {
                        $str[] = \Yii::t('main', $role); 
                    }
                    return implode(", ", $str);
                }
            ],
            [
                'attribute' => 'mobile',
                'label' => \Yii::t('main', 'Mobile'),
                'value' => function($model){
                    return '+998 '.$model['mobile'];
                }
            ],
            [
                'attribute' => 'created_at',
                'label' => \Yii::t('main', 'created_at'),
                'format' => ['date', 'php:Y-m-d H:m:s']
            ],
            [
                'attribute' => 'updated_at',
                'label' => \Yii::t('main', 'updated_at'),
                'format' => ['date', 'php:Y-m-d H:m:s']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => ' {reset} {update} {delete}',
                'buttons' => [
                    'update' => function($url, $model) use ($school) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['staff/update', 'school_type' => $school->type, 'school_id' => $school->id, 'id' => $model['user_id']]);
                    },
                    'delete' => function($url, $model) use ($school) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['staff/delete', 'school_type' => $school->type, 'school_id' => $school->id, 'id' => $model['user_id']], ['data' => ['confirm'=>Yii::t('main', 'Are you sure to delete this user?')]]);
                    },
                    'reset' => function($url, $model) use ($school) {
                        return Html::a('<span class="glyphicon glyphicon-lock"></span>', '#');
                    }
                ],
            ],
        ]
    ]
);
Pjax::end();
?>