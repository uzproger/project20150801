<?php 

/* School staff form */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->render('//school/menu', ['school' => $school, 'current' => 'staff']);

?>
<p>&nbsp;</p>
<?php $form = ActiveForm::begin([
    'id' => 'school-form', 
    'layout' => 'horizontal',
    'enableClientScript' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-2',
            'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-sm-6',
        ],
    ],
]); ?>
<?= $form->field($model, 'lastname') ?>
<?= $form->field($model, 'firstname') ?>
<?= $form->field($model, 'middlename') ?>
<?= $form->field($model, 'mobile', ['inputTemplate' => "<div class=\"input-group\"><div class=\"input-group-addon\">+998</div>{input}</div>"]) ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'roles')->checkboxList($model->getSchoolRoles(), ['item' => function($index, $label, $name, $checked, $value) use ($notAllowedRoles, $model) {
        $options = ['label' => $label, 'value' => $value];
        $checked = false;
        if (is_array($notAllowedRoles) && in_array($value, $notAllowedRoles)) {
            $options['disabled'] = true;
        }

        if (is_array($model->roles) && in_array($value, $model->roles)) {
            $checked = true;
        }
        $checkbox  = Html::beginTag('div', ['class' => 'checkbox']);
        $checkbox .= Html::beginTag('label');
        $checkbox .= Html::checkbox($name, $checked, $options);
        $checkbox .= Html::endTag('label');
        $checkbox .= Html::endTag('div');
    return $checkbox;
}]) ?>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::a('Отменить', ['staff/index', 'school_type' => $school->type, 'school_id' => $school->id], ['class' => 'btn btn-default', 'name' => 'cancel-button']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>