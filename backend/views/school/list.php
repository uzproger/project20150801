<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


$this->title = \Yii::t('main', $type);

$this->params['top_buttons'] = [
    [
        'label' => 'Добавить',
        'url' => ['/school/create', 'type' => $searchModel->type],
        'options' => ['class' => 'btn btn-success btn-sm pull-right']
    ]
];

Pjax::begin();
echo GridView::widget(
    [
        'id' => 'schools-grid',
        'summary' => '',
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'format' => 'raw',
                'label' => 'Название',
                'value' => function ($model) {
                    return Html::a($model['name'], ['/school/view', 'id' => $model['id'], 'type' => $model['type']], ['data-pjax' => 0]);
                }
            ],
            [
                'attribute' => 'number',
                'format' => 'raw',
                'label' => 'Номер',
                'value' => function ($model) {
                    return Html::a($model['number'], ['/school/view', 'id' => $model['id'], 'type' => $model['type']], ['data-pjax' => 0]);
                }
            ],
            [
                'attribute' => 'phone',
                'label' => 'Телефон'
            ],
            [
                'attribute' => 'fax',
                'label' => 'Факс'
            ],
            [
                'attribute' => 'email',
                'label' => 'Email адрес'
            ],
            [
                'attribute' => 'region',
                'format' => 'raw',
                'label' => 'Регион',
                'value' => function ($model) {
                    return $model['region'].', '.$model['city'];
                }
            ],
        ]
    ]
);
Pjax::end();
?>