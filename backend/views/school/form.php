<?php

use common\components\helpers\WordHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = $model->getTitle();

$this->render('//school/menu', ['school' => $model, 'current' => 'basic']);

?>
<p>&nbsp;</p>
<?php $form = ActiveForm::begin([
    'id' => 'school-form', 
    'layout' => 'horizontal',
    'enableClientScript' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-2',
            'wrapper' => 'col-sm-6',
        ],
    ],
]); ?>
<?= $form->field($model, 'number') ?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'phone') ?>
<?= $form->field($model, 'fax') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'website') ?>
<?= $form->field($model, 'inn') ?>
<?= $form->field($model, 'okonx') ?>
<?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::merge(['0' => 'Выберите регион'], $regions), ['id' => 'regions']) ?>
<?= $form->field($model, 'city_id')->dropDownList([], ['id' => 'city','disabled' => 'true']) ?>
<?= Html::input('hidden','selectedCity', isset($model->city->id) ? $model->city->id : 0, ['id'=>'selectedCity']) ?>
<?php if (!$model->isNewRecord) { ?>
<?= $form->field($model, 'type')->dropDownList($model->getSchoolTypes()) ?>
<?php } ?>
<?= $form->field($model, 'address')->textarea() ?>
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-6">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::a('Отменить', ($model->isNewRecord ? ['school/index', 'type' => WordHelper::pluralize($model->type)] : ['school/view', 'id' => $model->id, 'type' => $model->type]), ['class' => 'btn btn-default', 'name' => 'cancel-button']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>