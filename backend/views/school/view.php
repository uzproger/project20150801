<?php 

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->getTitle();
$this->render('//school/menu', ['school' => $model, 'current' => 'basic']);
?>
<p>&nbsp;</p>
<div class="table detail-view-table">
    <?php
        echo DetailView::widget([
            'model' => $model,
            'options' => [
                'class' => 'table table-striped table-bordered',
            ],
            'attributes' => [
                'name',
                'number',
                'phone',
                'fax',
                'email',
                'inn',
                'okonx',
                'website',
                'address',
                [
                    'label' => \Yii::t('main', 'Region'),
                    'value' => $model->city->parent->name.', '.$model->city->name
                ],
            ],
        ]);
    ?> 
    <?= Html::a('Изменить', ['/school/update', 'id' => $model->id, 'type' => $type], ['class' => 'btn btn-primary', 'name' => 'cancel-button']) ?>   
</div>