<?php

use backend\components\widgets\TabMenuWidget;
use common\components\helpers\WordHelper;

$this->title = $school->getTitle();

$this->params['top_buttons'] = [
    [
        'label' => \Yii::t('main', WordHelper::pluralize($school->type).'_list'),
        'url' => ['/school/index', 'type' => WordHelper::pluralize($school->type)],
        'options' => ['class' => 'btn btn-default btn-sm pull-right']
    ]
];

$this->params['tabs'] = [
    [
        'label'  => 'Общая информация',
        'link'   => ['school/view', 'id' => $school->id, 'type' => WordHelper::singularize($school->type)],
        'active' => $current == 'basic' ? true : false,
    ],
    [
        'label' => 'Сотрудники ('.$school->countStaff().')',
        'link'  => (!$school->isNewRecord ? ['staff/index', 'school_type' => $school->type, 'school_id' => $school->id] : '#'),
        'active' => $current == 'staff' ? true : false,
    ],
    [
        'label' => 'Банковские реквизиты ('.$school->getBankDetails()->count().')',
        'link'  => (!$school->isNewRecord ? ['bank/index', 'school_type' => $school->type, 'school_id' => $school->id] : '#'),
        'active' => $current == 'bank' ? true : false,
    ],
    [
        'label' => 'Классы',
        'active' => $current == 'class' ? true : false,
    ],
];

?>