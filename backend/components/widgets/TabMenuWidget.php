<?php 

namespace backend\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * 
 */
class TabMenuWidget extends Widget
{
    /**
     * Tab menus
     * @var array
     */
    public $menus = [];

    /**
     * Active item
     * @var integer
     */
    private $activeIndex;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (empty($this->menus)) {
            return;
        }

        $tabs = Html::beginTag('ul', ['class' => 'nav nav-tabs', 'encode' => false]);
        foreach ($this->menus as $menu) {
            if (isset($menu['active']) && $menu['active'] == true){
                $tabs .= Html::tag('li', Html::a($menu['label'], '#'), ['class' => 'active']);
            } else {
                $tabs .= Html::tag('li', Html::a($menu['label'], isset($menu['link']) ? $menu['link'] : '#'));
            }   
        }
        $tabs .= Html::endTag('ul');

        echo $tabs;
    }
}