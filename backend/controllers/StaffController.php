<?php

namespace backend\controllers;

use backend\models\SchoolStaffSearch;
use backend\models\SchoolStaffProfile;
use yii\web\NotFoundHttpException;


/**
 * 
 */
class StaffController extends SchoolBaseController
{
    /**
     * Staff list
     * @param  string $type School type
     * @param  string $id   School id
     */
    public function actionIndex($school_type, $school_id)
    {
        $searchStaff = new SchoolStaffSearch(['school_id' => $school_id]);

        return $this->render('list', [
            'school' => $this->school,
            'schoolStaffProvider' => $searchStaff->search(),
        ]);
    }

    public function actionCreate($school_type, $school_id)
    {
        $model = new SchoolStaffProfile(['school_id' => $school_id]);
        
        if ($model->load(\Yii::$app->request->post()) && $model->create()) {  
            $this->redirect(['staff/index', 'school_id' => $school_id, 'school_type' => $school_type]);  
        }

        return $this->render('form', [
            'school' => $this->school,
            'model' => $model,
            'notAllowedRoles' => $model->getNotAllowedRoles(),
        ]);
    }

    public function actionUpdate($school_type, $school_id, $id)
    {
        $model = (new SchoolStaffSearch(['school_id' => $school_id]))->searchOne(['id' => $id]);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($model->load(\Yii::$app->request->post()) && $model->updateProfile()) { 
            $this->redirect(['staff/index', 'school_id' => $school_id, 'school_type' => $school_type]);
        }

        return $this->render('form', [
            'school' => $this->school,
            'model' => $model,
            'notAllowedRoles' => $model->getNotAllowedRoles(),
        ]);
    }

    public function actionDelete($school_type, $school_id, $id)
    {
        $model = (new SchoolStaffSearch(['school_id' => $school_id]))->searchOne(['id' => $id]);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($model->deleteProfile()) { 
            $this->redirect(['staff/index', 'school_id' => $school_id, 'school_type' => $school_type]);
        }
    }
}