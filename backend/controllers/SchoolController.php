<?php

namespace backend\controllers;

use backend\models\Region;
use backend\models\School;
use backend\models\SchoolSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * 
 */
class SchoolController extends Controller
{
    /**
     * School list
     * @param  string $type School type
     */
    public function actionIndex($type)
    {
        $searchModel = new SchoolSearch(['type' => $type]);
        $dataProvider = $searchModel->search();

        return $this->render(
            'list', 
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'type' => $type,
            ]
        );
    }

    /**
     * Creates school, college, lyceum, university
     * @param  string $type School type
     */
    public function actionCreate($type)
    {
        $model = new School(['type' => $type]);
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->status = School::STATUS_ACTIVE;
            if ($model->makeRoot()) {
                $this->redirect(['/'.$model->type.'/'.$model->id]);  
            }
        }

        return $this->render(
            'form', 
            [
                'regions' => ArrayHelper::map(Region::getRegions(), 'id', 'name'),
                'model' => $model
            ]
        );
    }

    /**
     * Update school infor
     * @param  string $type School type
     * @param  integer $id School id
     */
    public function actionUpdate($type, $id)
    {
        $model = School::find()->where([
            'id' => $id, 
            'type' => $type,
            'status' => School::STATUS_ACTIVE,
        ])->with('city')->one();

        if (!$model) {
            throw new NotFoundHttpException;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save(false)) {
                $this->redirect(['/'.$model->type.'/'.$model->id]);  
            }
        }

        $model->region_id = $model->city->parent_id;

        return $this->render(
            'form', 
            [
                'regions' => ArrayHelper::map(Region::getRegions(), 'id', 'name'),
                'model' => $model
            ]
        );
    }

    /**
     * View schol basic info
     * @param  string $type School type: school, college, lyceum, university...
     * @param  integer $id School id
     */
    public function actionView($type, $id)
    {
        $model = School::find()->where([
            'id' => $id, 
            'type' => $type,
            'status' => School::STATUS_ACTIVE,
        ])->with('city.parent')->one();

        if (!$model) {
            throw new NotFoundHttpException;
        }

        return $this->render(
            'view',
            [
                'type' => $type,
                'model' => $model,
            ]
        );
    }

}