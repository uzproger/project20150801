<?php

namespace backend\controllers;

use backend\models\BankDetailSearch;
use backend\models\School;
use common\models\BankDetail;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * 
 */
abstract class SchoolBaseController extends Controller
{
    /**
     * @var School 
     */
    protected $school;

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)){
            return false;
        }

        $school_id = \Yii::$app->request->get('school_id');
        $school_type = \Yii::$app->request->get('school_type');

        $school = School::find()->where(['id' => $school_id, 'type' => $school_type])->one();
        if (!$school) {
            throw new NotFoundHttpException();
        }

        $this->school = $school;
        return true;
    }
}