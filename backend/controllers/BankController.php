<?php

namespace backend\controllers;

use backend\models\BankDetailSearch;
use common\models\BankDetail;
use yii\web\NotFoundHttpException;

/**
 * 
 */
class BankController extends SchoolBaseController
{
    /**
     * Bank list
     * @param  string $type School type
     * @param  string $id   School id
     */
    public function actionIndex($school_type, $school_id)
    {
        $searchBank1 = new BankDetailSearch(['school_id' => $school_id]);
        $searchBank2 = new BankDetailSearch(['school_id' => $school_id, 'is_treasury' => 1]);

        return $this->render('list', [
            'school' => $this->school,
            'schoolBankProvider' => $searchBank1->search(),
            'treasuryBankProvider' => $searchBank2->search(),
        ]);
    }


    public function actionCreate($school_type, $school_id, $treasury = 0)
    {
        $model = new BankDetail(['school_id' => $school_id]);

        if ($treasury == 1) {
            $model->is_treasury = 1;
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $this->redirect(['bank/index', 'school_id' => $school_id, 'school_type' => $school_type]);  
            }
        }

        return $this->render('form', [
            'school' => $this->school,
            'model' => $model
        ]);
    }

    public function actionUpdate($school_type, $school_id, $id)
    {
        $model = BankDetail::find()->where(['id' => $id, 'school_id' => $school_id])->one();

        if (!$model) {
            throw new \NotFoundHttpException;
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $this->redirect(['bank/index', 'school_id' => $school_id, 'school_type' => $school_type]);  
            }
        }
        return $this->render('form', [
            'school' => $this->school,
            'model' => $model
        ]);
    }
}