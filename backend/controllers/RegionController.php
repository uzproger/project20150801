<?php

namespace backend\controllers;

use common\components\helpers\WordHelper;
use backend\models\EntitySearch;
use backend\models\Entity;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * 
 */
class RegionController extends Controller
{
    /**
     * Region list
     */
    public function actionIndex($type, $region = 0)
    {
        $searchModel = new EntitySearch(['type' => $type]);

        if ($region > 0) {
            $region = Entity::find()->where(['id' => $region, 'type' => Entity::TYPE_REGION])->one();
            if (!$region) {
                throw new NotFoundHttpException;
            }
            $searchModel->parent_id = $region->id;
        } else {
            $searchModel->parent_id = $region;
        }

        $dataProvider = $searchModel->search();

        return $this->render(
            'list', 
            [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'region' => $region,
                'type' => $type,
            ]
        );
    }

    public function actionCreate($type, $region = 0)
    {
        if ($region > 0) {
            $region = Entity::find()->where(['id' => $region, 'type' => Entity::TYPE_REGION])->one();
            if (!$region) {
                throw new NotFoundHttpException;
            }
        }

        $model = new Entity(['type' => $type]);

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->parent_id = $model->type == Entity::TYPE_CITY ? $region->id : 0;
            if ($model->save()) {
                $redirect = $model->type == Entity::TYPE_CITY ? ['region/index', 'region' => $region->id, 'type' => 'cities'] : ['region/index', 'type' => 'regions'];
                $this->redirect($redirect);  
            }
        }

        return $this->render('form', [
            'model' => $model,
            'region' => $region
        ]);
    }

    public function actionUpdate($id, $type)
    {
        $model = Entity::find()->where(['type' => $type, 'id' => $id])->with('parent')->one();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $redirect = $model->type == Entity::TYPE_CITY ? ['region/index', 'region' => $model->parent->id, 'type' => 'cities'] : ['region/index', 'type' => 'regions'];
                $this->redirect($redirect);  
            }
        }

        return $this->render('form', [
            'model' => $model,
            'region' => $model->parent,
        ]);
    }

    /**
     * Get cities of region
     * @return string
     */
    public function actionCities()
    {
        $regionID = \Yii::$app->request->post('region');

        if ($regionID) {

            $region = Entity::find($regionID)->where([
                'type' => Entity::TYPE_REGION,
                'id'   => $regionID,
            ])->one();

            if ($region) {
                $cities = Entity::find()->select('id, name')->where([
                    'type' => Entity::TYPE_CITY,
                    'parent_id' => $region->id
                ])->orderBy('name')->asArray()->all();

                return json_encode($cities);
            }
        }

        return null;
    }
}