<?php

namespace backend\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * 
 */
class SubjectController extends Controller
{
    
    public function actionIndex()
    {
        return $this->render('list');
    }
}