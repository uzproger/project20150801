<?php

use yii\db\Schema;
use yii\db\Migration;

class m150425_125511_school_bank_detail extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school_bank_detail}}', [
            'id' => Schema::TYPE_PK,
            'name' => 'VARCHAR(64) NOT NULL',
            'treasury_name' => 'VARCHAR(64) NULL',
            'number' => 'CHAR(30) NOT NULL',
            'mfo' => 'CHAR(30) NOT NULL',
            'inn' => 'CHAR(30) NULL',
            'is_treasury' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
            'school_id' => 'INT(10) UNSIGNED NOT NULL',
            'created_at' => 'INT(10) UNSIGNED NOT NULL',
            'updated_at' => 'INT(10) UNSIGNED NOT NULL'
        ], $tableOptions);

        // indexes
        $this->createIndex('school_idx', '{{%school_bank_detail}}', 'school_id');
    }

    public function down()
    {
        $this->dropTable('{{%school_bank_detail}}');
    }
}
