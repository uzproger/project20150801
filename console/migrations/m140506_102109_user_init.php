<?php

use common\components\rbac\DbManager;
use common\models\User;
use yii\db\Schema;
use yii\db\Migration;

class m140506_102109_user_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'firstname' => 'CHAR(32) NOT NULL',
            'lastname' => 'CHAR(32) NOT NULL',
            'middlename' => 'CHAR(32) NULL',
            'phone' => 'CHAR(15) NULL',
            'mobile' => 'CHAR(15) NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);


        $auth = \Yii::$app->getAuthManager();
        $adminRole = $auth->getRole('admin');
        if ($adminRole) {
            $admin = new User();
            $admin->username = 'admin';
            $admin->firstname = 'Super';
            $admin->lastname = 'Admin';
            $admin->email = 'admin@admin.com';
            $admin->status = User::STATUS_ACTIVE;
            $admin->setPassword('123');
            $admin->getAuthKey();
            if ($admin->save(false)) {
                $auth->assign($adminRole, $admin->id);
                echo "    admin user has been created\n";
            }

        } else {
            echo "    Admin role was not found\n";
        }

        $teacherRole = $auth->getRole('teacher');
        if ($teacherRole) {
            $user = new User();
            $user->username = 'teacher';
            $user->firstname = 'Test';
            $user->lastname = 'Teacher';
            $user->email = 'admin@admin.com';
            $user->status = User::STATUS_ACTIVE;
            $user->setPassword('123');
            $user->getAuthKey();
            if ($user->save(false)) {
                $auth->assign($teacherRole, $user->id);
                echo "    test teacher has been created\n";
            }

        } else {
            echo "    Teacher role was not found\n";
        }
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
