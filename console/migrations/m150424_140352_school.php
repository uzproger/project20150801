<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_140352_school extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school}}', [
            'id' => Schema::TYPE_PK,
            'tree' => 'INT(11) UNSIGNED NOT NULL',
            'lft' => 'INT(11) UNSIGNED NOT NULL',
            'rgt' => 'INT(11) UNSIGNED NOT NULL',
            'depth' => 'INT(11) UNSIGNED NOT NULL',
            'type' => 'CHAR(10) NOT NULL',
            'status' => 'TINYINT(1) UNSIGNED NOT NULL',
            'name' => 'VARCHAR(100) NOT NULL',
            'number' => 'CHAR(30) NULL',
            'major' => 'SMALLINT(11) UNSIGNED NULL',
            'city_id' => 'SMALLINT(11) UNSIGNED NULL',
            'address' => 'TEXT NULL',
            'phone' => 'CHAR(30) NULL',
            'fax' => 'CHAR(30) NULL',
            'email' => 'CHAR(30) NULL',
            'inn' => 'CHAR(10) NULL',
            'okonx' => 'CHAR(10) NULL',
            'website' => 'CHAR(30) NULL',
            'created_at' => 'INT(10) UNSIGNED NOT NULL',
            'updated_at' => 'INT(10) UNSIGNED NOT NULL'
        ], $tableOptions);

        // indexes
        $this->createIndex('major_idx', '{{%school}}', 'major');
        $this->createIndex('city_idx', '{{%school}}', 'city_id');
        $this->createIndex('type', '{{%school}}', 'type');
    }

    public function down()
    {
        $this->dropTable('{{%school}}');
    }
    
}
