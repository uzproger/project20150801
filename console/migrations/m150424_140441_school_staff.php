<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_140441_school_staff extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school_staff}}', [
            'id' => Schema::TYPE_PK,
            'school_id' => 'INT(11) UNSIGNED NOT NULL',
            'user_id' => 'INT(11) UNSIGNED NOT NULL',
            'type' => 'TINYINT(1) UNSIGNED NOT NULL',
            'created_at' => 'INT(10) UNSIGNED NOT NULL',
            'updated_at' => 'INT(10) UNSIGNED NOT NULL'
        ], $tableOptions);

        $this->createIndex('user_idx', '{{%school_staff}}', 'user_id');
        $this->createIndex('staff_unq', '{{%school_staff}}', ['school_id', 'user_id'], true);
    }

    public function down()
    {
        $this->dropTable('{{%school_staff}}');
    }
}
