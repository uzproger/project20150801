<?php

use common\models\User;
use yii\db\Schema;
use yii\db\Migration;
use yii\helpers\Console;
use yii\rbac\DbManager;

class m140506_102107_rbac_roles_init extends Migration
{
    /** @var yii\rbac\DbManager */
    private $auth;

    /**
     * Initilize auth manager
     * @return boolean
     */
    private function initAuthManager()
    {
        $authManager = \Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            $string = Console::ansiFormat('authManager', [Console::FG_YELLOW, Console::BOLD]);
            echo "    You should configure ".$string." component to use database before executing this migration. \n\n";
            return false;
        }

        $this->auth = $authManager;
        return true;
    }

    /**
     * @inheritdoc
     */
    public function up()
    {  
        echo "\n";
        if (!$this->initAuthManager()) {
            return false;
        }

        if ($this->db->schema->getTableSchema($this->auth->assignmentTable, true) === null || 
            $this->db->schema->getTableSchema($this->auth->itemChildTable, true) === null  ||
            $this->db->schema->getTableSchema($this->auth->itemTable, true) === null  ||
            $this->db->schema->getTableSchema($this->auth->ruleTable, true) === null ) {
            $string = Console::ansiFormat('php yii migrate --migrationPath=@yii/rbac/migrations', [Console::FG_YELLOW, Console::BOLD]);
            echo "    You should first run: ".$string." before executing this migration. \n\n";
            return false;
        }

        $defaultRoles = [
            'backend' => [
                [
                    'role' => 'operator',
                    'permissions' => [
                        [
                            'name' => 'backendLogin',
                            'description' => 'Allow login to backend end',
                        ],
                    ],
                    'childRoles' => [],
                ],
                [
                    'role' => 'admin',
                    'permissions' => [],
                    'childRoles' => ['operator'],
                ],
            ],
            'frontend' => [
                [
                    'role' => 'teacher',
                    'permissions' => [
                        [
                            'name' => 'frontendLogin',
                            'description' => 'Allow login to front end'
                        ],           
                    ],
                    'childRoles' => [],
                ],
                [
                    'role' => 'supervisor', 
                    'permissions' => [],
                    'childRoles' => ['teacher'],
                ],
                [
                    'role' => 'headmaster', 
                    'permissions' => [],
                    'childRoles' => ['teacher'],
                ],
                [
                    'role' => 'director',
                    'permissions' => [],
                    'childRoles' => ['teacher'],
                ],
            ]
        ];

        foreach ($defaultRoles as $group => $roles) {
            foreach ($roles as $key => $role) {
                if (($newRole = $this->createRole($role['role'], $group)) != null) {
                    if (isset($role['permissions']) && !empty($role['permissions']) && !$this->assignPermissions($newRole, $role['permissions'])) {
                        return false;
                    }

                    if (isset($role['childRoles']) && !empty($role['childRoles']) && !$this->assignChildRoles($newRole, $role['childRoles'])){
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Create role
     * @param  string $name
     * @return null|yii\rbac\Role
     */
    private function createRole($name, $group)
    {
        $role = $this->auth->createRole($name);
        $role->data = ['group' => $group];
        if ($this->auth->add($role)) {
            echo "    role: ".$role->name." has been created\n";
            return $role;
        }

        return null;
    }

    /**
     * Create permission
     * @param  array $data
     * @return null|yii\rbac\Permission
     */
    private function createPermission($data)
    {
        $permission = $this->auth->createPermission($data['name']);
        $permission->description = isset($data['description']) ? $data['description'] : '';
        if ($this->auth->add($permission)) {
            echo "    permission: ".$permission->name." has been created\n";
            return $permission;
        }
        echo "    permission creating was faile\n";
        return null;
    }

    /**
     * Assigns permissions into $role
     * @param  yii\rbac\Role $role
     * @param  array $permissions
     * @return boolean
     */
    private function assignPermissions($role, $permissions)
    {
        foreach ($permissions as $key => $permission) {
            if (($newPermission = $this->createPermission($permission)) != null) {
                if (!$this->auth->addChild($role, $newPermission)){
                    echo "    permission assignment was failed\n";
                    return false;
                    break;
                }
            }
        }

        return true;
    }

    /**
     * Assign child roles
     * @param  yii\rbac\Role $role
     * @param  array $childRoles
     * @return boolean
     */
    private function assignChildRoles($role, $childRoles)
    {
        foreach ($childRoles as $roleName) {
            $childRole = $this->auth->getRole($roleName);
            if (!$childRole) {
                echo "    wrong role: ".$roleName."\n";
                return false;
            }

            if (!$this->auth->addChild($role, $childRole)) {
                echo "    child role assignment was failed\n";
                return false;
            }

            echo "    child role has been assigned\n";
        }

        return true;
    }


    public function down()
    {
        echo "\n";
        if (!$this->initAuthManager()) {
            return false;
        }

        try {
            $this->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")->execute();
            $this->truncateTable($this->auth->itemChildTable);
            $this->truncateTable($this->auth->assignmentTable);
            $this->truncateTable($this->auth->itemTable);
            $this->truncateTable($this->auth->ruleTable);
            $this->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")->execute();
        } catch (\Exception $e) {
            echo "    failed\n";
            return false;
        }
        
        return true;
    }
}
