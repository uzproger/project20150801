<?php

use common\models\Entity;
use common\models\EntityGroup;
use yii\db\Schema;
use yii\db\Migration;

class m140506_102112_entity_group extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(EntityGroup::tableName(), [
            'entity_id' => 'INT(10) UNSIGNED NOT NULL',
            'group' => 'CHAR(10) NOT NULL',
            'FOREIGN KEY (entity_id) REFERENCES ' . Entity::tableName() . ' (id) ON DELETE CASCADE ON UPDATE CASCADE'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(EntityGroup::tableName());
    }
}
